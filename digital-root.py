def digital_root(n):
    s = sum([int(i) for i in str(n)])
    if s > 10:
        return digital_root(s)
    else:
        return s


print(digital_root(16))
print(digital_root(456))